module go.dev.pztrn.name/fastpastebin

go 1.16

require (
	github.com/alecthomas/chroma v0.8.2
	github.com/dchest/captcha v0.0.0-20200903113550-03f5f0333e1f
	github.com/dgrijalva/jwt-go v3.2.0+incompatible // indirect
	github.com/dlclark/regexp2 v1.4.0 // indirect
	github.com/go-sql-driver/mysql v1.5.0
	github.com/jmoiron/sqlx v1.2.0
	github.com/labstack/echo v3.3.10+incompatible
	github.com/labstack/gommon v0.3.0 // indirect
	github.com/lib/pq v1.9.0
	github.com/pressly/goose v2.6.0+incompatible
	github.com/rs/zerolog v1.20.0
	go.dev.pztrn.name/flagger v0.0.0-20200617193309-89bc9818b76c
	golang.org/x/crypto v0.0.0-20201221181555-eec23a3978ad
	golang.org/x/net v0.0.0-20201224014010-6772e930b67b
	gopkg.in/yaml.v2 v2.4.0
	mvdan.cc/gofumpt v0.1.1 // indirect
)
